<?php
/*
 * @author Shukurullo Odilov <shukurullo0321@gmail.com>
 * @link telegram: https://t.me/odilov_web
 * @date 01.11.2022, 09:25
 */

namespace OdilovSh\LaravelTmUploadable\Facades;

use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Facade;
use OdilovSh\LaravelTmUploadable\ImageQuality;
use OdilovSh\LaravelTmUploadable\Services\S3Uploader;
use OdilovSh\LaravelTmUploadable\Contracts\Uploader as UploaderContract;

/**
 * Uploader facade
 *
 * @see UploaderContract::upload()
 * @method static array upload(UploadedFile|string $file, string $category, $itemId, string $type, string $fileName = null)
 *
 * @see UploaderContract::uploadImage()
 * @method static array uploadImage(UploadedFile|string $file, string $category, $itemId, string $fileName = null)
 *
 * @see UploaderContract::uploadDocument()
 * @method static array uploadDocument(UploadedFile|string $file, string $category, $itemId, string $fileName = null)
 *
 * @see UploaderContract::uploadByUrl()
 * @method static array uploadByUrl(string $url, string $category, $itemId, string $type, string $fileName = null)
 *
 * @see UploaderContract::uploadImageByUrl()
 * @method static array uploadImageByUrl(string $url, string $category, $itemId, string $fileName = null)
 *
 * @see UploaderContract::uploadDocumentByUrl()
 * @method static array uploadDocumentByUrl(string $url, string $category, $itemId, string $fileName = null)
 *
 * @see UploaderContract::deleteFile()
 * @method static void deleteFile(mixed $id)
 *
 * @see UploaderContract::checkIfAttributeHasOldFile()
 * @method static void checkIfAttributeHasOldFile(mixed $value)
 *
 * @see UploaderContract::getFileUrl()
 * @method static string getFileUrl(mixed $value)
 *
 * @see UploaderContract::getImageUrl()
 * @method static void getImageUrl(mixed $value, ImageQuality $quality = ImageQuality::ORIGINAL)
 *
 * @see UploaderContract
 */
class Uploader extends Facade
{

    /**
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return config('tm-uploadable.service_class');
    }

}
