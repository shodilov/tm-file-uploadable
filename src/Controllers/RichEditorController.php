<?php
/*
 * @author Shukurullo Odilov <shukurullo0321@gmail.com>
 * @link telegram: https://t.me/odilov_web
 * @date 01.11.2022, 14:31
 */

namespace OdilovSh\LaravelTmUploadable\Controllers;

use Illuminate\Contracts\Filesystem\FileNotFoundException;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Support\Facades\Http;
use OdilovSh\LaravelTmUploadable\Services\FileManagerUploader;

class RichEditorController extends BaseController
{

    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    /**
     * @param Request $request
     * @return string
     * @throws FileNotFoundException
     */
    public function upload(Request $request)
    {

        if ($request->hasFile('upload')) {

            $file = $request->file('upload');
            $url = FileManagerUploader::uploaderUrl('/api/rich');
            $rich = Http::attach("file", $file->get(), $file->getClientOriginalName())
                ->acceptJson()
                ->post($url);

            $url = FileManagerUploader::getterUrl('/' . $rich->json('data')['url']);

            return response()->json([
                'default' => $url,
                '500' => $url,
            ]);
        }

        return '';
    }

}
