<?php
/*
 * @author Shukurullo Odilov <shukurullo0321@gmail.com>
 * @link telegram: https://t.me/odilov_web
 * @date 01.11.2022, 11:38
 */

namespace OdilovSh\LaravelTmUploadable\Rules;

use Illuminate\Validation\Rules\ImageFile;

class ImageRule extends ImageFile
{

    /**
     * @param array $config
     */
    public function __construct(array $config = [])
    {
        $rules = config('tm-uploadable.rules.image', []);
        $rules = array_merge($rules, $config);

        foreach ($rules as $property => $value) {
            $this->$property = $value;
        }

        parent::__construct();
    }

}
