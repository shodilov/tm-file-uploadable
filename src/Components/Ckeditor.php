<?php

namespace OdilovSh\LaravelTmUploadable\Components;

use Illuminate\View\Component;

class Ckeditor extends Component
{

    /**
     * @var int
     */
    protected static int $idCount = 0;

    /**
     * @var string
     */
    protected static string $idPrefix = 'tm-ckeditor-';

    /**
     * @var string
     */
    public $id;

    /**
     * @var array
     */
    public array $options;

    /**
     * @var string
     */
    public $name;

    /**
     * @var string
     */
    public $label;

    /**
     * @var string
     */
    public $errorAttribute;

    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct($id = null, $name = null, $label = null, array $options = [], $errorAttribute = null)
    {
        $this->id = $this->getId($id);
        $this->name = $name;
        $this->label = $label ?? \Str::title($name);
        $this->options = $this->renderOptions($options);
        $this->errorAttribute = $errorAttribute ?? $name;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        return view('tm-uploadable::components.ckeditor');
    }

    /**
     * @param array $options
     * @return array
     */
    private function renderOptions(array $options): array
    {
        $defaultOptions = [];
        return array_merge($defaultOptions, $options);
    }

    /**
     * @return string
     */
    public function generateNewId()
    {
        $id = static::$idPrefix . self::$idCount;
        self::$idCount++;
        return $id;
    }

    /**
     * @param mixed $id
     * @return mixed|string
     */
    public function getId($id)
    {
        return empty($id) ? $this->generateNewId() : $id;
    }

}
