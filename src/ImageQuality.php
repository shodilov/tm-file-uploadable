<?php
/*
 * @author Shukurullo Odilov <shukurullo0321@gmail.com>
 * @link telegram: https://t.me/odilov_web
 * @date 31.10.2022, 15:15
 */

namespace OdilovSh\LaravelTmUploadable;

enum ImageQuality: string
{
    case SMALL = 'small';
    case MEDIUM = 'medium';
    case HD = 'hd';
    case ORIGINAL = 'original';

    public function width(): int
    {
        return match ($this) {
            self::SMALL => 200,
            self::MEDIUM => 400,
            self::HD, self::ORIGINAL => 0,
        };
    }

    /**
     * @return int
     */
    public function quality(): int
    {
        return match ($this) {
            self::SMALL => 70,
            self::MEDIUM => 80,
            self::HD => 90,
            default => 100,
        };
    }

}
