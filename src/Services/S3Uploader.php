<?php

namespace OdilovSh\LaravelTmUploadable\Services;

use OdilovSh\LaravelTmUploadable\ImageQuality;
use OdilovSh\LaravelTmUploadable\Services\Uploader as AbstractUploader;
use OdilovSh\LaravelTmUploadable\Traits\Scalable;
use Storage;

class S3Uploader extends AbstractUploader
{

    use Scalable;

    /**
     * @param mixed $file
     * @param string $path
     * @param mixed $itemId
     * @param string $type
     * @param string $fileName
     * @param string $originalExtension
     * @return mixed
     */
    public function sendUploadRequest(mixed $file, string $path, mixed $itemId, string $type, string $fileName, string $originalExtension, ?string $encodingExtension = null): mixed
    {
        $folder = $path . '/' . $itemId;

        try {
            Storage::disk('s3')->deleteDirectory($folder);
        } catch (\Exception $e) {
            app('log')->error($e->getMessage());
        }

        $filePath = $folder . '/' . $fileName . '.' . $originalExtension;
        $result = Storage::disk('s3')->put($filePath, $file, 'public');

        if (!$result) {
            return [
                'success' => false,
                'message' => 'An error occurred while uploading the file to s3 server'
            ];
        }

        $localUrl = 'public/' . $filePath;
        $local = Storage::put($localUrl, $file, 'public');
        $scaledImages = [];

        $encodingExtension = $encodingExtension ?: $originalExtension;

        if ($local) {
            if ($type == self::TYPE_IMAGE) {
                $scaledImages = $this->uploadScaledImages($localUrl, $folder, $fileName, $encodingExtension);
            }
        }

        $bucket = config('filesystems.disks.s3.bucket');

        return [
            'success' => true,
            'data'    => [
                'bucket'    => $bucket,
                'folder'    => $folder,
                'file'      => $fileName,
                'extension' => $originalExtension,
                'images'    => $scaledImages,
            ]
        ];
    }

    /**
     * @param array $data
     * @return mixed
     */
    public function normalizeImageResult(mixed $data): mixed
    {
        return json_encode($data);
    }

    /**
     * @param array $data
     * @return mixed
     */
    public function normalizeDocumentResult(mixed $data): mixed
    {
        return json_encode($data);
    }

    public function deleteFile(mixed $value)
    {
        if (!is_array($value)) {
            $value = json_decode($value, true);
        }

        if (!is_array($value) || empty($value)) {
            return;
        }

        $folder = $value['folder'] ?? '';

        if ($folder) {
            try{
                Storage::disk('s3')->deleteDirectory($folder);
            }
            catch (\Exception $e) {
                app('log')->error($e->getMessage());
            }
        }
    }

    public function checkIfAttributeHasOldFile(mixed $value): bool
    {
        return !empty($value);
    }

    public function getFileUrl(mixed $value): ?string
    {
        if (!is_array($value)) {
            $value = json_decode($value, true);
        }

        if (!is_array($value)) {
            return $value;
        }

        if (empty($value)) {
            return null;
        }

        $bucket = $value['bucket'] ?? '';
        $folder = $value['folder'] ?? '';
        $file = $value['file'] ?? '';
        $extension = $value['extension'] ?? '';

        return config('filesystems.disks.s3.url') . '/' . $bucket . '/' . $folder . '/' . $file . '.' . $extension;

    }

    public function getImageUrl(mixed $value, ImageQuality $quality = ImageQuality::ORIGINAL): ?string
    {
        if (!is_array($value)) {
            $value = json_decode($value, true);
        }

        if (!is_array($value)) {
            return $value;
        }

        if (empty($value)) {
            return null;
        }

        $path = data_get($value, ['images', $quality->value]);
        $bucket = $value['bucket'] ?? '';
        $folder = $value['folder'] ?? '';
        $file = $value['file'] ?? '';
        $extension = $value['extension'] ?? '';

        if (!$path) {
            $path = $folder . '/' . $file . '.' . $extension;;
        }

        return config('filesystems.disks.s3.url') . '/' . $bucket . '/' . $path;
    }
}
