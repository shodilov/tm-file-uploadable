<?php
/*
 * @author Shukurullo Odilov <shukurullo0321@gmail.com>
 * @link telegram: https://t.me/odilov_web
 * @date 01.11.2022, 09:22
 */

namespace OdilovSh\LaravelTmUploadable\Services;

use Arr;
use Http;
use OdilovSh\LaravelTmUploadable\ImageQuality;
use OdilovSh\LaravelTmUploadable\Services\Uploader as AbstractUploader;

class FileManagerUploader extends AbstractUploader
{

    //<editor-fold desc="Get Urls" defaultstate="collapsed">

    /**
     * @param string|null $path
     * @return mixed
     */
    public static function uploaderUrl(string $path = null): mixed
    {
        $url = config('tm-uploadable.url.upload');
        if ($path) {
            $url .= $path;
        }
        return $url;
    }

    /**
     * @param string|null $path
     * @return mixed
     */
    public static function getterUrl(string $path = null): mixed
    {
        $url = config('tm-uploadable.url.get');
        if ($path) {
            $url .= $path;
        }
        return $url;
    }

    //</editor-fold>

    protected function sendUploadRequest(mixed $file, string $path, mixed $itemId, string $type, string $fileName, string $originalExtension): mixed
    {

        $url = self::uploaderUrl('/api/files');

        return Http::attach('file', $file, $fileName)
            ->acceptJson()
            ->timeout(180)
            ->post($url, [
                'file_type' => $type,
                'category'  => $path,
                'itemId'    => $itemId,
            ])->json();
    }

    /**
     * Delete file from file manager
     * @param mixed $value
     */
    public function deleteFile(mixed $value)
    {
        if (is_array($value)) {
            if (isset($value['id'])) {
                $id = $value['id'];
                $url = self::uploaderUrl('/api/files/' . $id);
                Http::acceptJson()->post($url, ['_method' => 'DELETE']);
            }
        }
    }

    /**
     * @param mixed $data
     * @return mixed
     */
    protected function normalizeImageResult(mixed $data): mixed
    {
        $file_result = [];

        foreach ($data['file'] as $files) {
            $name = $files['name'];
            $array = explode('.', $name);
            $file_result[$array[0]] = [
                'id'        => $files['id'],
                'name'      => $array[0],
                'extension' => $array[1],
                'url'       => $files['url'],
            ];
        }

        return [
            'id'          => $data['id'],
            'folder_type' => $data['folder_type'],
            'images'      => $file_result,
        ];
    }

    /**
     * @param mixed $data
     * @return mixed
     */
    protected function normalizeDocumentResult(mixed $data): mixed
    {
        $file = $data['file'][0];
        return [
            'id'          => $data['id'],
            'folder_type' => $data['folder_type'],
            'file'        => [
                'id'   => $file['id'],
                'name' => $file['name'],
                'url'  => $file['url'],
            ]
        ];
    }

    /**
     * @param mixed $value
     * @return bool
     */
    public function checkIfAttributeHasOldFile(mixed $value): bool
    {
        return is_array($value) && isset($value['id']);
    }

    public function getFileUrl(mixed $value): ?string
    {
        if (!is_array($value)) {
            $value = json_decode($value, true);
        }

        if (is_array($value)) {
            $path = Arr::get($value, 'file.url');
            if ($path) {
                return FileManagerUploader::getterUrl('/' . $path);
            }
            return '';
        }

        return $value;
    }

    public function getImageUrl(mixed $value, ImageQuality $quality = ImageQuality::ORIGINAL): ?string
    {

        if (!is_array($value)) {
            $value = json_decode($value, true);
        }

        if (is_array($value)) {

            // agar ushbu attribute document shaklida yuklangan bo'lsa
            // u holda document urlni qaytaramiz
            // sababi ba'zi hollarda (masalan svg kengaytmali) rasmlarni
            // ham document shaklida yuklangan bo'lishi mn.
            if (!array_key_exists('images', $value)) {
                return $this->getFileUrl($value);
            }

            $images = $value['images'];
            $path = data_get($images, [$quality->value, 'url']);

            // agar berilgan qualitydagi rasm topilmasa,
            // u holda original rasmni qaytariladi.
            if (!$path) {
                $path = data_get($images, [ImageQuality::ORIGINAL->value, 'url']);
            }

            if ($path) {
                return FileManagerUploader::getterUrl('/' . $path);
            }

            return '';
        }
        return $value;
    }
}
