<?php

namespace OdilovSh\LaravelTmUploadable\Services;

use Illuminate\Contracts\Filesystem\FileNotFoundException;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Str;
use OdilovSh\LaravelTmUploadable\Contracts\Uploader as UploaderContract;
use Symfony\Component\CssSelector\Exception\InternalErrorException;
use Throwable;

abstract class Uploader implements UploaderContract
{

    const TYPE_IMAGE = 'image';
    const TYPE_DOCUMENT = 'document';

    //<editor-fold desc="Upload" defaultstate="collapsed">

    /**
     * @param UploadedFile|string $file
     * @param string $path
     * @param mixed $itemId
     * @param string $type
     * @param string|null $fileName
     * @param string|null $encodingExtension
     * @return null|array
     * @throws FileNotFoundException
     * @throws InternalErrorException
     * @throws Throwable
     */
    public function upload(mixed $file, string $path, mixed $itemId, string $type, ?string $fileName = null, ?string $encodingExtension = null): mixed
    {

        if (empty($file)) {
            return null;
        }

        if ($file instanceof UploadedFile) {
            if ($fileName === null) {
                $fileName = $file->getClientOriginalName();
            }
            $file = $file->get();
        }

        $originalExtension = $this->parseFileExtension($fileName);

        throw_if(empty($originalExtension), 'File extension must not be empty');

        if ($type == self::TYPE_IMAGE) {

            // agar yuklanayotgan rasm svg formatida bo'lsa
            // biz uni document sifatida yuklaymiz
            // sababi fayl manager svg ni rasm sifatida
            // parse qilolmayapti! :(
            if ($originalExtension == 'svg') {
                $type = self::TYPE_DOCUMENT;
            }
        }

        $fileName = Str::uuid();

        $result = $this->sendUploadRequest($file, $path, $itemId, $type, $fileName, $originalExtension, $encodingExtension);
        $success = data_get($result, 'success', false);

        if ($success && isset($result['data'])) {
            return $this->normalizeResult($result['data'], $type);
        } else {
            throw new InternalErrorException(json_encode($result));
        }
    }

    /**
     * @param string|resource $file
     * @param string $path
     * @param mixed $itemId
     * @param string $type
     * @param string $fileName
     * @param string $originalExtension
     * @param string|null $encodingExtension
     * @return mixed
     */
    abstract protected function sendUploadRequest(mixed $file, string $path, mixed $itemId, string $type, string $fileName, string $originalExtension, ?string $encodingExtension = null): mixed;

    /**
     * @param string|null $fileName
     * @return string
     */
    protected function parseFileExtension(?string $fileName): string
    {
        $array = explode('.', $fileName);
        if (count($array) > 1) {
            return end($array);
        }
        return '';
    }

    /**
     * @param $file string|UploadedFile
     * @param string|null $path
     * @param mixed $itemId
     * @param string|null $fileName
     * @return null|array
     * @throws FileNotFoundException
     * @throws InternalErrorException
     * @throws Throwable
     */
    public function uploadImage(mixed $file, ?string $path, mixed $itemId, ?string $fileName): mixed
    {
        return $this->upload($file, $path, $itemId, self::TYPE_IMAGE, $fileName);
    }

    /**
     * @param $file UploadedFile|string
     * @param string $path
     * @param mixed $itemId
     * @param string|null $fileName
     * @return null|array
     * @throws FileNotFoundException
     * @throws InternalErrorException
     * @throws Throwable
     */
    public function uploadDocument(mixed $file, string $path, mixed $itemId, ?string $fileName = null): mixed
    {
        return $this->upload($file, $path, $itemId, self::TYPE_DOCUMENT, $fileName);
    }

    //</editor-fold>

    //<editor-fold desc="Upload by url" defaultstate="collapsed">

    /**
     * @param string $url
     * @param string $path
     * @param mixed $itemId
     * @param string $type
     * @param string|null $fileName
     * @return null|array
     * @throws Throwable
     */
    public function uploadByUrl(string $url, string $path, mixed $itemId, string $type, ?string $fileName = null): mixed
    {
        if (is_null($fileName)) {
            $fileName = $this->getFileNameByUrl($url);
        }
        if (!$this->fileExists($url)) {
            return null;
        }

        $file = file_get_contents($url);
        if (!$file) {
            return null;
        }
        return $this->upload($file, $path, $itemId, $type, $fileName);
    }

    /**
     * @param string $url
     * @return string
     */
    protected function getFileNameByUrl(string $url): string
    {
        $array = explode('/', $url);
        return end($array);
    }

    /**
     * @param string $url
     * @return bool
     */
    public function fileExists(string $url): bool
    {
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_NOBODY, true);
        curl_exec($ch);
        $response_code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch);
        return $response_code == 200;
    }

    /**
     * @param string $url
     * @param string $path
     * @param mixed $itemId
     * @param string|null $fileName
     * @return null|array
     * @throws FileNotFoundException
     * @throws InternalErrorException
     * @throws Throwable
     */
    public function uploadImageByUrl(string $url, string $path, mixed $itemId, ?string $fileName = null): mixed
    {
        return $this->uploadByUrl($url, $path, $itemId, self::TYPE_IMAGE, $fileName);
    }

    /**
     * @param string $url
     * @param string $path
     * @param mixed $itemId
     * @param string|null $fileName
     * @return null|array
     * @throws FileNotFoundException
     * @throws InternalErrorException
     * @throws Throwable
     */
    public function uploadDocumentByUrl(string $url, string $path, mixed $itemId, ?string $fileName = null): mixed
    {
        return $this->uploadByUrl($url, $path, $itemId, self::TYPE_DOCUMENT, $fileName);
    }

    //</editor-fold>

    //<editor-fold desc="Normalize result" defaultstate="collapsed">

    /**
     * @param mixed $data
     * @param string $type
     * @return array
     */
    protected function normalizeResult(mixed $data, string $type): mixed
    {
        if ($type == self::TYPE_IMAGE) {
            return $this->normalizeImageResult($data);
        }
        return $this->normalizeDocumentResult($data);
    }

    /**
     * @param mixed $data
     * @return mixed
     */
    abstract protected function normalizeImageResult(mixed $data): mixed;

    /**
     * @param mixed $data
     * @return mixed
     */
    abstract protected function normalizeDocumentResult(mixed $data): mixed;

    //</editor-fold>

}
