<?php

namespace OdilovSh\LaravelTmUploadable\Traits;


use Intervention\Image\Facades\Image;
use OdilovSh\LaravelTmUploadable\ImageQuality;
use Storage;

trait Scalable
{

    /**
     * @param string $file
     * @param string|null $folder
     * @param string $fileName
     * @param string $extension
     * @return array
     */
    protected function uploadScaledImages(mixed $file, ?string $folder, string $fileName, string $extension): array
    {
        $cases = ImageQuality::cases();
        $items = [];

        foreach ($cases as $case) {

            if ($case == ImageQuality::ORIGINAL) {
                continue;
            }

            $result = $this->uploadScaledImage($case, $file, $folder, $fileName, $extension);
            if ($result) {
                $items[$case->value] = $result;
            }
        }
        return $items;
    }

    /**
     * @param ImageQuality $imageQuality
     * @param string $file
     * @param string|null $folder
     * @param string $fileName
     * @param string $extension
     * @return bool
     */
    protected function uploadScaledImage(ImageQuality $imageQuality, mixed $file, ?string $folder, string $fileName, string $extension): mixed
    {

        if ($extension == 'svg') {
            return null;
        }

        $file = Storage::get($file);
        $img = Image::make($file);

        $width = $imageQuality->width();

        if ($imageQuality != ImageQuality::ORIGINAL && $width > 0) {
            $img->widen($width);
        }

        $img->encode($extension, $imageQuality->quality());
        $content = $img->getEncoded();
        $filePath = $folder . '/' . $fileName . '-' . $imageQuality->value . '.' . $extension;

        if (Storage::disk('s3')->put($filePath, $content, 'public')) {
            return $filePath;
        }
        return null;
    }

}
