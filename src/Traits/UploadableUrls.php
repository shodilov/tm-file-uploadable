<?php
/*
 * @author Shukurullo Odilov <shukurullo0321@gmail.com>
 * @link telegram: https://t.me/odilov_web
 * @date 01.11.2022, 10:02
 */

namespace OdilovSh\LaravelTmUploadable\Traits;

use OdilovSh\LaravelTmUploadable\Facades\Uploader;
use OdilovSh\LaravelTmUploadable\ImageQuality;

trait UploadableUrls
{

    public function attributeDocumentUrl(string $attribute)
    {
        $value = $this->$attribute;
        return Uploader::getFileUrl($value);
    }

    /**
     * @param string $attribute
     * @param ImageQuality $quality
     * @return string|null
     * @see ImageQuality for image qulaties
     */
    public function attributeImageUrl(string $attribute, ImageQuality $quality = ImageQuality::ORIGINAL): ?string
    {
        $value = $this->$attribute;
        return Uploader::getImageUrl($value, $quality);
    }

    /**
     * @param string $attribute
     * @return null|string
     */
    public function attributeOriginalImageUrl(string $attribute): ?string
    {
        return $this->attributeImageUrl($attribute, ImageQuality::ORIGINAL);
    }

    /**
     * @param string $attribute
     * @return null|string
     */
    public function attributeHdImageUrl(string $attribute): ?string
    {
        return $this->attributeImageUrl($attribute, ImageQuality::HD);
    }

    /**
     * @param string $attribute
     * @return null|string
     */
    public function attributeMediumImageUrl(string $attribute): ?string
    {
        return $this->attributeImageUrl($attribute, ImageQuality::MEDIUM);
    }

    /**
     * @param string $attribute
     * @return null|string
     */
    public function attributeSmallImageUrl(string $attribute): ?string
    {
        return $this->attributeImageUrl($attribute, ImageQuality::SMALL);
    }
}
