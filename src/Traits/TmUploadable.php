<?php
/*
 * @author Shukurullo Odilov <shukurullo0321@gmail.com>
 * @link telegram: https://t.me/odilov_web
 * @date 31.10.2022, 14:33
 */

namespace OdilovSh\LaravelTmUploadable\Traits;

use Illuminate\Http\UploadedFile;
use OdilovSh\LaravelTmUploadable\Facades\Uploader;

trait TmUploadable
{

    use UploadableUrls;

    /**
     * @return void
     */
    public static function bootTmUploadable()
    {
        static::deleting(function ($model) {
            /** @var static $model */
            $model->deleteAllFilesFromFileManager();
        });

    }

    /**
     * @return array
     * Example:
     *```php
     *      return [
     *            'main_image' => [
     *                'type' => 'image',
     *                'category' => 'slider-main-image',
     *            ],
     *            'portfolio' => [
     *                'type' => 'document',
     *                'category' => 'user-portfolio',
     *            ],
     *      ];
     *```
     */
    abstract function uploadableSettings(): array;

    //<editor-fold desc="Upload Files" defaultstate="collapsed">

    /**
     * @return void
     */
    public function uploadFilesToFileManager()
    {

        $attributes = $this->uploadableSettings();
        $request = request();

        foreach ($attributes as $attribute => $settings) {

            $file = $request->file($attribute);

            if (!$file) {
                continue;
            }

            $result = $this->uploadNewFileToFileManager($file, $settings);
            $this->{$attribute} = $result;
            $this->save();
        }
    }

    /**
     * @param $attribute
     * @return bool
     */
    public function checkIfAttributeHasOldFile($attribute): bool
    {
        $value = $this->$attribute;
        return Uploader::checkIfAttributeHasOldFile($value);
    }

    /**
     * @param UploadedFile $file
     * @param $settings
     * @return array
     */
    public function uploadNewFileToFileManager($file, $settings)
    {
        return Uploader::upload($file, $settings['category'], $this->id, $settings['type'], null, $settings['encodingExtension'] ?? null);
    }

    //</editor-fold>

    //<editor-fold desc="Delete files" defaultstate="collapsed">

    /**
     * @return void
     */
    public function deleteAllFilesFromFileManager(): void
    {
        $settings = $this->uploadableSettings();
        foreach ($settings as $attribute => $setting) {
            $this->deleteFileFromFileManager($attribute);
        }
    }

    /**
     * @param string $attribute
     */
    public function deleteFileFromFileManager(string $attribute)
    {
        $value = $this->$attribute;
        Uploader::deleteFile($value);
    }

    //</editor-fold>

}
