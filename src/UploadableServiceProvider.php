<?php

namespace OdilovSh\LaravelTmUploadable;

use Blade;
use Illuminate\Support\ServiceProvider;

class UploadableServiceProvider extends ServiceProvider
{

    public function boot()
    {
        $this->publishes([
            __DIR__ . '/../config/tm-uploadable.php' => config_path('tm-uploadable.php')
        ]);

        $this->publishes([
            __DIR__ . '/../public' => public_path('vendor/tm-uploadable'),
        ], 'public');

        $this->loadRoutesFrom(__DIR__ . '/../routes/uploadable.php');

        $this->loadViewsFrom(__DIR__ . '/../views', 'tm-uploadable');

        $this->publishes([
            __DIR__ . '/../views' => resource_path('views/vendor/tm-uploadable'),
        ]);

        Blade::componentNamespace('OdilovSh\\LaravelTmUploadable\\Components', 'tm-uploadable');
    }

}
