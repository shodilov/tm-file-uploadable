<?php

namespace OdilovSh\LaravelTmUploadable\Contracts;

use Illuminate\Http\UploadedFile;
use OdilovSh\LaravelTmUploadable\ImageQuality;

interface Uploader
{
    /**
     * @param string|UploadedFile $file
     * @param string $path
     * @param mixed $itemId
     * @param string $type
     * @param string|null $fileName
     * @param string|null $encodingExtension File extension. It is only supported when file type is 'image'
     * @return mixed
     */
    public function upload(mixed $file, string $path, mixed $itemId, string $type, ?string $fileName = null, ?string $encodingExtension = null): mixed;

    /**
     * @param string|UploadedFile $file
     * @param string|null $path
     * @param mixed $itemId
     * @param string|null $fileName
     * @return mixed
     */
    public function uploadImage(mixed $file, ?string $path, mixed $itemId, ?string $fileName): mixed;

    /**
     * @param string|UploadedFile $file
     * @param string $path
     * @param mixed $itemId
     * @param string|null $fileName
     * @return mixed
     */
    public function uploadDocument(mixed $file, string $path, mixed $itemId, ?string $fileName = null): mixed;

    /**
     * @param string $url
     * @param string $path
     * @param mixed $itemId
     * @param string $type
     * @param string|null $fileName
     * @return mixed
     */
    public function uploadByUrl(string $url, string $path, mixed $itemId, string $type, ?string $fileName = null): mixed;

    /**
     * @param string $url
     * @param string $path
     * @param mixed $itemId
     * @param string|null $fileName
     * @return mixed
     */
    public function uploadImageByUrl(string $url, string $path, mixed $itemId, ?string $fileName = null): mixed;

    /**
     * @param string $url
     * @param string $path
     * @param mixed $itemId
     * @param string|null $fileName
     * @return mixed
     */
    public function uploadDocumentByUrl(string $url, string $path, mixed $itemId, ?string $fileName = null): mixed;

    /**
     * @param mixed $value
     */
    public function deleteFile(mixed $value);

    /**
     * @param mixed $value
     * @return bool
     */
    public function checkIfAttributeHasOldFile(mixed $value): bool;

    /**
     * @param mixed $value
     * @return string|null
     */
    public function getFileUrl(mixed $value): ?string;

    /**
     * @param mixed $value
     * @param ImageQuality $quality
     * @return string|null
     */
    public function getImageUrl(mixed $value, ImageQuality $quality = ImageQuality::ORIGINAL): ?string;

}
