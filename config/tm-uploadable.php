<?php

return [
    'service_class' => \OdilovSh\LaravelTmUploadable\Services\S3Uploader::class,
    'url' => [
        'upload' => env('TM_UPLOADABLE_UPLOAD_URL'),
        'get' => env('TM_UPLOADABLE_GETTER_URL'),

    ],
    'rules' => [
        'image' => [
            'allowedMimetypes' => [
                'jpeg',
                'png',
                'svg+xml',
                'svg',
                'webp',
            ],
            'maximumFileSize' => 500, // in kilobytes
        ],
        'file' => [
            'maximumFileSize' => 1024, // in kilobytes
           // 'allowedMimetypes' => ['pdf', 'doc', 'docx', 'xls', 'xlsx', 'ppt', 'pptx', 'txt', 'csv', 'zip', 'rar',  'jpg', 'jpeg', 'png', 'svg+xml', 'svg', 'webp'],
        ]
    ]
];
