<?php
/*
 * @author Shukurullo Odilov <shukurullo0321@gmail.com>
 * @link telegram: https://t.me/odilov_web
 * @date 01.11.2022, 14:33
 */

use OdilovSh\LaravelTmUploadable\Controllers\RichEditorController;

Route::post('/uploadable/rich-editor/upload', [RichEditorController::class, 'upload'])
    ->middleware([
        'throttle:api',
        \Illuminate\Routing\Middleware\SubstituteBindings::class,
    ])
    ->name('uploadable.rich-editor.upload');
