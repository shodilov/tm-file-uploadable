### About

This package provides a simple way to upload files to a local or remote server.
As well this package can upload images from Ckeditor rich text editor.

### Documentation
You can find the full documentation [here](https://wiki-portal.texnomart.uz/books/mikroservisy/page/uploadable-package-fayl-upload-paketi)

### Envirement variables
Your .env file must have the following variables:
```dotenv
TM_UPLOADABLE_UPLOAD_URL="https://upload.example.com"
TM_UPLOADABLE_GETTER_URL="https://get.example.com"
```
